import React from "react";
import FormFields from "./../../components/FormFields";
import Welcome from "./../../components/Welcome";

import { GlobalStyle, Main } from "./styles";

function Home() {
  return (
    <>
      <GlobalStyle />
      <Main>
        <Welcome />
        <FormFields />
      </Main>
    </>
  );
}

export default Home;
