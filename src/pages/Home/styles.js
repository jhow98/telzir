import { createGlobalStyle } from "styled-components";
import styled from "styled-components";
import * as palette from "./../../utils/variables";

export const GlobalStyle = createGlobalStyle`
body {
  margin: 0;
  font-family: 'Segoe UI', 'Roboto', 'Oxygen',
    'Ubuntu', 'Cantarell', 'Fira Sans', 'Droid Sans', 'Helvetica Neue',
    sans-serif !important;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
}

html, body, #root {
  height: 100%;
  background-color: ${palette.SECONDARY_COLOR};
}
`;

// eslint-disable-next-line import/prefer-default-export
export const Main = styled.main`
  display: flex;
  flex-direction: row;

  @media(max-width: 724px){
    flex-direction: column;
  }
`;
