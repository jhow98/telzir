export const constValues = [
  {
    from: '011',
    to: '016',
    value: 1.90
  },

  {
    from: '016',
    to: '011',
    value: 2.90
  },

  {
    from: '011',
    to: '017',
    value: 1.70
  },

  {
    from: '017',
    to: '011',
    value: 2.70
  },

  {
    from: '011',
    to: '018',
    value: 0.90
  },

  {
    from: '018',
    to: '011',
    value: 1.90
  },
]



