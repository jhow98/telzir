import calcReducer from "./calcReducer";
import { combineReducers } from "redux";
export const Reducers = combineReducers({
  calcAction: calcReducer,
});
