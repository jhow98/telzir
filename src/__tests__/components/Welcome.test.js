import React from 'react';
import Adapter from 'enzyme-adapter-react-16';
import { shallow, configure } from 'enzyme';
import Welcome from '../../components/Welcome';


configure({ adapter: new Adapter() });
describe('Welcome', () => {
  it('should render the Welcome component correctly in "debug" mode', () => {
    const component = shallow(<Welcome debug />);
    expect(component).toMatchSnapshot();
  });
});



