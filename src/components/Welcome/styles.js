import styled from "styled-components";
import * as palette from "./../../utils/variables";

// eslint-disable-next-line import/prefer-default-export
export const WelcomeAside = styled.div`
  background-color: ${palette.PRIMARY_COLOR};
  width: 50vw;
  padding: 40px;
  display: flex;
  flex-direction: column;
  justify-content: space-evenly;
  align-items: center;
  align-content: center;

  @media (max-width: 724px) {
    height: auto;
    width: 100vw;
  }
`;

export const WelcomeText = styled.div`
  h1,
  h2 {
    color: ${palette.WHITE_COLOR};
    text-align: center;
  }

  h1 {
    font-weight: 600;
    font-size: 1.8rem;
  }

  h2 {
    font-weight: 300;
    font-size: 1.4rem;
  }

  @media (max-width: 724px) {
    h1,
    h2 {
    }
  }
`;

export const Figure = styled.figure`
  display: flex;
  justify-content: center;
  align-content: center;
  max-height: 50vh;
  img {
    width: 80%;
    max-width: 500px;
  }

  @media (max-width: 724px) {
    img {
      width: 60%;
      margin-top: 30px;
    }

    @media (max-width: 724px) {
      img {
        display: none;
      }
    }
  }
`;
