import { constValues } from "./values";
import { TAX } from "./consts";
import PropTypes from "prop-types";

export const discountedValue = (from, to, time, plan) => {
  let totalPrice = 0;
  if (time < plan) {
    return 0;
  } else {
    const exceeded = time - plan;
    Object.values(constValues).map((value) => {
      if (value.from === from && value.to === to) {
        let newValue = value.value * TAX * exceeded;
        let callCost = value.value * exceeded;
        totalPrice = newValue + callCost;
      }
      return true;
    });
  }
  return totalPrice;
};

export const totalPrice = (from, to, time) => {
  let totalPrice = 0;
  Object.values(constValues).map((value) => {
    if (value.from === from && value.to === to) {
      totalPrice = time * value.value;
    }
    return true;
  });
  return totalPrice;
};

discountedValue.propTypes = {
  from: PropTypes.number,
  to: PropTypes.number,
  time: PropTypes.number,
  plan: PropTypes.number,
};

totalPrice.propTypes = {
  from: PropTypes.number,
  to: PropTypes.number,
  time: PropTypes.number,
};
