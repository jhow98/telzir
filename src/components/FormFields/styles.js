import styled from "styled-components";
import * as palette from "./../../utils/variables";

// eslint-disable-next-line import/prefer-default-export
export const FormAside = styled.div`
  background-color: ${palette.SECONDARY_COLOR};
  width: 50vw;
  min-height: 100vh;
  padding: 40px;
  display: flex;
  flex-direction: column;
  flex-wrap: wrap;
  justify-content: space-evenly;
  align-items: center;
  align-content: center;

  @media (max-width: 724px) {
    justify-content: flex-start;

    height: auto;
    width: 100vw;
  }
`;

export const FormTitle = styled.h2`
  color: ${palette.BLACK_COLOR};
  text-align: center;
  font-weight: 300;

  @media (max-width: 724px) {
    font-size: 1.6rem;
    margin-bottom: 40px;
  }
`;

export const Form = styled.form`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  align-content: center;
  width: 100%;
`;

export const Label = styled.label`
  margin-bottom: 0.5em;
  color: ${palette.PRIMARY_COLOR};
  display: block;
  font-weight: 600;
`;

export const NumberInput = styled.input`
  padding: 1em;
  font-size: 1.5rem;
  color: ${palette.BLACK_COLOR};
  background: ${palette.WHITE_COLOR};
  border: none;
  border-radius: 3px;
  width: 350px;
  margin-bottom: 0.5em;

  @media (max-width: 724px) {
    width: 250px;
    font-size: 1rem;
  }
`;

export const SelectInput = styled.select`
  padding: 1em;
  font-size: 1.5rem;
  color: ${palette.BLACK_COLOR};
  background: ${palette.WHITE_COLOR};
  border: none;
  border-radius: 3px;
  width: 350px;
  margin-bottom: 0.5em;

  -webkit-appearance: none;
  -moz-appearance: none;
  appearance: none;

  @media (max-width: 724px) {
    width: 250px;
    font-size: 1rem;
  }
`;

export const ButtonInput = styled.input`
  padding: 1em;
  font-size: 0.8rem;
  color: ${palette.WHITE_COLOR};
  background: ${palette.PRIMARY_COLOR};
  border: none;
  border-radius: 3px;
  width: 350px;
  margin-bottom: 0.5em;
  font-weight: bold;
  text-transform: uppercase;
  letter-spacing: 1px;

  &:hover {
    background: ${palette.WHITE_COLOR};
    color: ${palette.PRIMARY_COLOR};
    transition: all 0.2s ease;
  }
  @media (max-width: 724px) {
    width: 250px;
    font-size: 1rem;
  }
`;
