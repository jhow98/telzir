import React from "react";
import PropTypes from "prop-types";
import { useSelector } from "react-redux";

import { Container } from "./styles";

const Result = () => {
  const { normalPrice, planPrice } = useSelector((state) => state.calcAction);

  if (planPrice || normalPrice) {
    return (
      <Container data-testid="result-container">
        <div>
          <p>
            Custo da ligação <strong>com o FaleMais</strong>:
          </p>
          <p>R$ {parseFloat(planPrice).toFixed(2)}</p>
        </div>
        <div>
          <p>
            Custo da ligação <strong>sem o FaleMais</strong>:{" "}
          </p>
          <p>R$ {parseFloat(normalPrice).toFixed(2)}</p>
        </div>
      </Container>
    );
  } else
    return (
      <Container data-testid="result-container">
        <div>
          <h3>Verifique as informações digitadas.</h3>
          <h6>Não são permitidas ligações entre os DDD's selecionados.</h6>
        </div>
      </Container>
    );
};

export default Result;

Result.propTypes = {
  planPrice: PropTypes.number,
  normalPrice: PropTypes.number,
};
