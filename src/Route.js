import React from "react";
import Home from "./pages/Home";
import { Provider } from "react-redux";
import { Store } from "./store";

const Route = () => {
  return (
    <Provider store={Store}>
      <Home />
    </Provider>
  );
};

export default Route;
