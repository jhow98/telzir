## Sobre o projeto

O projeto Telzir utiliza, dentre outros, os seguintes recursos:

- [Redux](https://redux.js.org/basics/usage-with-react)
- [Syled Components](https://styled-components.com/)
- [@testing-library/react](https://testing-library.com/docs/react-testing-library/intro)
- [Jest](https://jestjs.io/)
- [Prop Types](https://www.npmjs.com/package/prop-types)
- [Bootstrap](https://getbootstrap.com/)

## Instalação em ambiente de desenvolvimento

Executar os seguintes comandos:

1. yarn
2. yarn dev

## Testes

Para executar os testes da Home e componentes, executar o comando abaixo:

yarn test

## License

O React tem sua licença open-source [MIT license](https://opensource.org/licenses/MIT).

## CI/CD

A rotina de CI/CD irá ser executada da seguinte maneira:

1. Commit no branch develop: Build, Testes e Deploy em ambiente de testes (staging). URL: https://telzir-test-staging.herokuapp.com/
2. Commit no master : Build, Testes e Deploy em ambiente de produção. URL: https://telzir-test-production.herokuapp.com/
3. Commit nos demais branches: Build e Testes
