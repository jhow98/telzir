import styled from "styled-components";
import * as palette from "./../../utils/variables";

// eslint-disable-next-line import/prefer-default-export
export const Container = styled.div`
  background-color: ${palette.WHITE_COLOR};
  padding: 25px;
  width: 80%;
  min-height: 80px;
  margin-top: 50px;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  align-content: center;

  @media(max-width: 724px){
    flex-direction: column;
    height: auto;
    width: 90%;
  }

  box-shadow: 5px 7px 5px -7px rgba(0, 0, 0, 0.44);
  -webkit-box-shadow: 5px 7px 5px -7px rgba(0, 0, 0, 0.44);
  -moz-box-shadow: 5px 7px 5px -7px rgba(0, 0, 0, 0.44);

  div {
    padding: 20px;
    p {
      color: ${palette.PRIMARY_COLOR};
      text-align: left;
    }
  }
`;
