export const BLACK_COLOR = '#0B0C10';
export const WHITE_COLOR = '#F4F4F4';
export const PRIMARY_COLOR = '#1F2833';
export const SECONDARY_COLOR = '#C5C6C7';
