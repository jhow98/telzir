import React from "react";
import calling from "./../../assets/images/calling.svg";
import { WelcomeAside, WelcomeText, Figure } from "./styles";

const Welcome = () => {
  return (
    <WelcomeAside>
      <WelcomeText>
        <h1>Bem vindo à Telzir.</h1>
        <h2>Simulações de custos com ligações em tempo real</h2>
      </WelcomeText>

      <Figure>
        <img
          src={calling}
          alt="calling figure"
          title="Liberdade para saber quanto vai gastar!"
        />
      </Figure>
    </WelcomeAside>
  );
};

export default Welcome;
