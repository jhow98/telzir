import React from "react";
import { render, fireEvent } from "@testing-library/react";
import Adapter from 'enzyme-adapter-react-16';
import { shallow, configure } from 'enzyme';

import FormFields from "../../components/FormFields";

jest.mock("react-redux");
configure({ adapter: new Adapter() });
describe("Form component", () => {
  it('should render the Form component correctly in "debug" mode', () => {
    const component = shallow(<FormFields debug />);
    expect(component).toMatchSnapshot();
  });

  it("should be able to complete the form", () => {
    const { getByLabelText } = render(<FormFields />);

    fireEvent.change(
      getByLabelText("Selecione o DDD de destino", { target: { value: "011" } })
    );
    fireEvent.change(
      getByLabelText("Selecione o DDD de destino", { target: { value: "016" } })
    );
    fireEvent.change(
      getByLabelText("Tempo em Minutos", { target: { value: 20 } })
    );
    fireEvent.change(
      getByLabelText("Selecione o Plano", { target: { value: 30 } })
    );
  });
});
