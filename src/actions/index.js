import { CALC_VALUES } from "../actions/actionTypes";

const calcAction = (normalPrice, planPrice, error) => ({
  type: CALC_VALUES,
  payload: { normalPrice, planPrice },
});

export default calcAction;