import { CALC_VALUES } from "../actions/actionTypes";

const initialState = {
  normalPrice: 0,
  planPrice: 0,
};
const calcReducer = (state = initialState, action) => {
  switch (action.type) {
    case CALC_VALUES:
      return {
        ...state,
        planPrice: action.payload.planPrice,
        normalPrice: action.payload.normalPrice,
      };
    default:
      return state;
  }
};

export default calcReducer;
