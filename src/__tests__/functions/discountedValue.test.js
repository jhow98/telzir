import { discountedValue } from "./../../utils/calcFunctions";

//from, to, time
test("Should be return the correct values for all example cases", () => {
  expect(discountedValue("011", "016", 20, 30)).toBe(0);
  expect(discountedValue("011", "017", 80, 60)).toBe(37.4);
  expect(discountedValue("018", "011", 200, 120)).toBe(167.2);
  expect(discountedValue("018", "017", 100, 30)).toBe(0);
});
