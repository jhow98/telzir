import React, { useState } from "react";
import { useDispatch } from "react-redux";
import Result from "../../components/Result";
import calcAction from "./../../actions/index";
import { totalPrice, discountedValue } from "../../utils/calcFunctions";
import {
  FormAside,
  FormTitle,
  Form,
  Label,
  NumberInput,
  SelectInput,
  ButtonInput,
} from "./styles";


const FormFields = () => {
  const [from, setFrom] = useState("");
  const [to, setTo] = useState("");
  const [duration, setDuration] = useState("");
  const [plan, setPlan] = useState("");
  const [formSubmited, setFormSubmited] = useState(false);


  const clearForm = () => {
    setFrom("");
    setTo("");
    setDuration("");
    setPlan("");
  };

  const dispatch = useDispatch();

  const handleCalc = (event) => {
    event.preventDefault();
    const fullPriceResult = totalPrice(from, to, duration);
    const discountPriceResult = discountedValue(from, to, duration, plan);

    dispatch(calcAction(fullPriceResult, discountPriceResult));
    setFormSubmited(true)
    clearForm();
  };

  const handleChangeDDDTo = (event) => {
    setTo(event.target.value);
  };

  const handleChangeDDDFrom = (event) => {
    setFrom(event.target.value);
  };

  const handleChangePlan = (event) => {
    setPlan(event.target.value);
  };
  return (
    <FormAside>
      <FormTitle>Por favor, preencha os campos abaixo:</FormTitle>
      <Form onSubmit={(event) => handleCalc(event)} data-testid='calc-form'>
        <div>
          <Label htmlFor="ddd-from">Selecione o DDD de origem</Label>
          <SelectInput
            value={from}
            onChange={(e) => handleChangeDDDFrom(e)}
            name="ddd-from"
            id="ddd-from"
            required
          >
            <option value="">DDD de origem</option>
            <option value="011">011</option>
            <option value="016">016</option>
            <option value="017">017</option>
            <option value="018">018</option>
          </SelectInput>
        </div>

        <div>
          <Label htmlFor="ddd-to">Selecione o DDD de destino</Label>
          <SelectInput
            value={to}
            onChange={(e) => handleChangeDDDTo(e)}
            name="ddd-to"
            id="ddd-to"
            required
          >
            <option value="">DDD de destino</option>

            <option value="011">011</option>
            <option value="016">016</option>
            <option value="017">017</option>
            <option value="018">018</option>
          </SelectInput>
        </div>

        <div>
          <Label htmlFor="time">Tempo em Minutos</Label>
          <NumberInput
            required
            type="number"
            min="0"
            name="time"
            id="time"
            value={duration}
            onChange={(e) => setDuration(e.target.value)}
            placeholder="Tempo em minutos"
          />
        </div>

        <div>
          <Label htmlFor="plan">Selecione o Plano</Label>
          <SelectInput
            value={plan}
            onChange={(e) => handleChangePlan(e)}
            name="plan"
            id="plan"
          >
            <option value="">Selecione</option>
            <option value="30">Fale Mais 30</option>
            <option value="60">Fale Mais 60</option>
            <option value="120">Fale Mais 120</option>
          </SelectInput>
        </div>
        <ButtonInput type="submit" value="Calcular" />

        {formSubmited && <Result />}
        
      </Form>
    </FormAside>
  );
};

export default FormFields;

