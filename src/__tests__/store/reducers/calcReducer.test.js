import calcReducer, { initialState } from "./../../../reducers/calcReducer";
import calcAction from "./../../../actions/index";

describe("Calc reducer", () => {
  it("CALC_VALUES", () => {
    const state = calcReducer(
      initialState,
      calcAction(10, 20)
    );

    expect(state).toStrictEqual(
      {
        planPrice: 20,
        normalPrice: 10,
      },
    );
  });
});
