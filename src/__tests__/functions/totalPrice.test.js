import { totalPrice } from "./../../utils/calcFunctions";

//from, to, time, plan
test("Should be return the correct values for all example cases", () => {
  expect(totalPrice("011", "016", 20, 30)).toBe(38);
  expect(totalPrice("011", "017", 80, 60)).toBe(136);
  expect(totalPrice("018", "011", 200, 120)).toBe(380);
  expect(totalPrice("018", "017", 100, 30)).toBe(0);
});
